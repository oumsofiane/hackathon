'use strict';

angular.module('starter.services', [])

    .factory('UsersAPI', function($q, $http) {
        return {
            all: function() {
                var deferred = $q.defer();

                $http.defaults.headers.common.Authorization = 'Basic ' + btoa('hollyhaho@yahoo.com' + ':' + 'cacheme');
                $http.post('https://api-us.clusterpoint.com/100503' + '/users/_search.json', { 'query': '*', 'list': { 'document': 'yes' } })
                    .success(function(data/*, status, headers, config*/) {
                        deferred.resolve(data);
                    })
                    .error(function(/*data, status, headers, config*/) {
                        deferred.reject();
                    })
                ;

                return deferred.promise;
            },
            insert: function(user) {
                var deferred = $q.defer();

                $http.defaults.headers.common.Authorization = 'Basic ' + btoa('hollyhaho@yahoo.com' + ':' + 'cacheme');
                $http.post('https://api-us.clusterpoint.com/100503' + '/users.json', user)
                    .success(function(data/*, status, headers, config*/) {
                        deferred.resolve(data);
                    })
                    .error(function(/*data, status, headers, config*/) {
                        deferred.reject();
                    })
                ;

                return deferred.promise;
            },
            update: function(user) {
                var deferred = $q.defer();

                $http.defaults.headers.common.Authorization = 'Basic ' + btoa('hollyhaho@yahoo.com' + ':' + 'cacheme');
                $http.put('https://api-us.clusterpoint.com/100503' + '/users.json', user)
                    .success(function(data/*, status, headers, config*/) {
                        deferred.resolve(data);
                    })
                    .error(function(/*data, status, headers, config*/) {
                        deferred.reject();
                    })
                ;

                return deferred.promise;
            },
            remove: function(userId) {
                var deferred = $q.defer();

                $http.defaults.headers.common.Authorization = 'Basic ' + btoa('hollyhaho@yahoo.com' + ':' + 'cacheme');
                $http.delete('https://api-us.clusterpoint.com/100503' + '/users.json', { 'id': userId })
                    .success(function(data/*, status, headers, config*/) {
                        deferred.resolve(data);
                    })
                    .error(function(/*data, status, headers, config*/) {
                        deferred.reject();
                    })
                ;

                return deferred.promise;
            },
            get: function(userId) {
                var deferred = $q.defer();

                $http.defaults.headers.common.Authorization = 'Basic ' + btoa('hollyhaho@yahoo.com' + ':' + 'cacheme');
                $http.post('https://api-us.clusterpoint.com/100503' + '/users/_retrieve.json', { 'id': userId })
                    .success(function(data/*, status, headers, config*/) {
                        deferred.resolve(data.documents[0]);
                    })
                    .error(function(/*data, status, headers, config*/) {
                        deferred.reject();
                    })
                ;

                return deferred.promise;
            },
            getMultiple: function(users) {
                var deferred = $q.defer();

                $http.defaults.headers.common.Authorization = 'Basic ' + btoa('hollyhaho@yahoo.com' + ':' + 'cacheme');
                $http.post('https://api-us.clusterpoint.com/100503' + '/users/_retrieve.json', users)
                    .success(function(data/*, status, headers, config*/) {
                        deferred.resolve(data);
                    })
                    .error(function(/*data, status, headers, config*/) {
                        deferred.reject();
                    })
                ;

                return deferred.promise;
            },
            deleteTask: function(user, task){
                var taskIndex = user.ownTasks.indexOf(task);
                var tasks = user.ownTasks;
                tasks.splice(taskIndex, 1);
                user.ownTasks = tasks;

                this.update(user).then(function() {
                    //delete assignement
                    if(task.status == 'delegated'){
                        //var assignerUser = this.get(task.assignedByUser);
                        var that = this;
                        this.get(task.assignedByUser).then(function(assignerUser) {
                            var tasks = assignerUser.delegatedTasks;
                            var assignedTask = {id: task.id, assignedToUser: user.id, status: 'delegated'};
                            var assignedTaskIndex = assignerUser.delegatedTasks.indexOf(assignedTask);
                            assignerUser.delegatedTasks.splice(assignedTaskIndex, 1);

                            that.update(assignerUser);
                        });
                    }
                });

            }
        };
    })

    .factory('Configuration', function(){
        //var taskStatus = ['unassigned', 'assigned-to-me', 'delegated', 'done'];
        //var challengeStatus = ['in-progress', 'completed'];
        var configuration = {
            user: 1,
            challengeStatus: ['in-progress', 'completed'],
            taskStatus: ['in-progress', 'done'],
            debug: true
            }

        return{
            all: function() {
                return configuration;
            },
            log: function(logText){
                if(configuration.debug){
                    console.log(logText);
                }
            }
        }
    })

    .factory('Users', function() {
        // Might use a resource here that returns a JSON array

        // Some fake users
        var users = [{
            id: 0,
            name: 'Sofiane',
            profession: 'Developer',
            location: [34.0500, 118.2500],
            challenges:[{challengeId: 0, status: 'in-progress'}, {challengeId: 1, status: 'uncompleted'}],
            skills:[{id: 0, level: 0}],
            delegatedTasks:[{id: 1, assignedToUser: 1, status: 'done'}],
            ownTasks: [
                {
                    id: 0, assignedByUser: 1, status: 'my-task', response: null
                }
            ],
            connections: [{userId: 1, connection: true}, {userId: 2, connection: false}],
            face: 'https://pbs.twimg.com/profile_images/514549811765211136/9SgAuHeY.png'
        }, {
            id: 1,
            name: 'Maira',
            profession: 'Designer',
            location:[34.0500, 118.2500],
            skills: [{id: 0, level: 1}],
            delegatedTasks:[{id: 1, assignedToUser: 1, status: 'in-progress'}],
            ownTasks:[
                {id: 0, assignedByUser: 0, status: 'done', response: null},

                {id: 1, assignedByUser: 2, status: 'in-progress', response: null}
            ],
            connections: [{userId: 0, connection: true}, {userId: 2, connection: false}],
            face: 'https://avatars3.githubusercontent.com/u/11214?v=3&s=460'
        },{
            id: 2,
            name: 'Willem',
            profession: 'Manager',
            location:[34.0500, 118.2500],
            skills: [{id: 0, level: 1}],
            delegatedTasks:[{id: 1, assignedToUser: 1, status: 'delegated'}],
            ownTasks:[{id: 0, assignedByUser: 0, status: 'done'}],
            connections: [{userId: 0, connection: true}, {userId: 2, connection: false}],
            face: 'https://pbs.twimg.com/profile_images/479090794058379264/84TKj_qa.jpeg'
        }, {
            id: 3,
            name: 'Holly',
            profession: 'Developer',
            skills: [{id: 0, level: 1}],
            delegatedTasks:[{id: 1, assignedToUser: 1, status: 'delegated'}],
            ownTasks:[{id: 0, assignedByUser: 0, status: 'done'}],
            connections: [{userId: 0, connection: true}, {userId: 2, connection: false}],
            face: 'https://pbs.twimg.com/profile_images/598205061232103424/3j5HUXMY.png'
        }];

        return {
            all: function() {
                return users;
            },
            remove: function(user) {
                users.splice(users.indexOf(user), 1);
            },
            get: function(userId) {
                for (var i = 0; i < users.length; i++) {
                    if (users[i].id === parseInt(userId)) {
                        return users[i];
                    }
                }
                return null;
            },
            deleteTask: function(user, task){
                console.log(task);
                console.log(user);
                var userIndex = users.indexOf(user);
                console.log('User Index: ' + userIndex);
                var taskIndex = users[userIndex].ownTasks.indexOf(task);
                console.log('Task Index: ' + taskIndex.toString());

                console.log('Selected User');
                console.log(users[userIndex]);

                console.log('User Tasks Before Delete');
                console.log(users[userIndex].tasks);

                var tasks = users[userIndex].ownTasks;
                tasks.splice(taskIndex, 1);
                users[userIndex].ownTasks = tasks;
                console.log('User Tasks After Delete');
                console.log(tasks);

                //delete assignement
                if(task.status == 'delegated'){
                    var assignerUser = this.get(task.assignedByUser);
                    var tasks = assignerUser.delegatedTasks;
                    var assignedTask = {id: task.id, assignedToUser: user.id, status: 'delegated'}
                    var assignedTaskIndex = assignerUser.delegatedTasks.indexOf(assignedTask)
                    assignerUser.delegatedTasks.splice(assignedTaskIndex, 1);
                }

            },
            getConnections: function(userId)
            {
                for (var i = 0; i < users.length; i++)
                {
                    if (users[i].id === parseInt(userId))
                    {
                        return users[i].connections;
                    }
                }
            },
            getChallenges: function(userId)
            {
                for (var i = 0; i < users.length; i++)
                {
                    if (users[i].id === parseInt(userId))
                    {
                        return users[i].challenges;
                    }
                }
            },
            getLocation: function(userId)
            {
                for (var i = 0; i < users.length; i++)
                {
                    if (users[i].id === parseInt(userId))
                    {
                        return users[i].location;
                    }
                }
            },

            // checks if user has a task.
            hasTask: function(user, task){
                user.ownTasks.forEach(function(elem){
                    if(elem.id === task.id){
                        return true;
                    }
                })

                return false;
            },

            // returns the userTask object.
            getUserTask: function(user, task){
                console.log('getUserTask: ');
                console.log(user);
                user.ownTasks.forEach(function(elem){
                    if(elem.id == task.id){
                        return elem;
                    }
                })

                return false;
            },

            // an assignerUser delegates a task to a user user.
            delegateTask: function(task, user, assignerUser){
                var hasTask = this.hasTask(user, task);

                if(!hasTask){
                    var userIndex = users.indexOf(user);
                    var assigneeUser = users[userIndex];
                    assigneeUser.ownTasks.push({id: task.id, assignedByUser: 1, status: 'delegated'})

                    var assignerUserIndex = users.indexOf(assignerUser);
                    var assignerUser = users[assignerUserIndex];
                    assignerUser.delegatedTasks.push({id: task.id, assignedToUser: assigneeUser.id, status: 'delegated'})
                }
            }

        };
    })

    .factory('TasksAPI', function($q, $http) {
        return {
            getForChallenge: function (challengeId) {
                var deferred = $q.defer();

                $http.defaults.headers.common.Authorization = 'Basic ' + btoa('hollyhaho@yahoo.com' + ':' + 'cacheme');
                $http.post('https://api-us.clusterpoint.com/100503' + '/tasks/_search.json', { 'query': '<challengeId>' + challengeId + '</challengeId>', 'list': { 'document': 'yes' }, 'ordering': '<numeric><taskNumber>ascending,en</taskNumber></numeric>' })
                    .success(function(data/*, status, headers, config*/) {
                        deferred.resolve(data);
                    })
                    .error(function(/*data, status, headers, config*/) {
                        deferred.reject();
                    })
                ;

                return deferred.promise;
            },
            all: function() {
                var deferred = $q.defer();

                $http.defaults.headers.common.Authorization = 'Basic ' + btoa('hollyhaho@yahoo.com' + ':' + 'cacheme');
                $http.post('https://api-us.clusterpoint.com/100503' + '/tasks/_search.json', { 'query': '*', 'list': { 'document': 'yes' } })
                    .success(function(data/*, status, headers, config*/) {
                        deferred.resolve(data);
                    })
                    .error(function(/*data, status, headers, config*/) {
                        deferred.reject();
                    })
                ;

                return deferred.promise;
            },
            insert: function(task) {
                var deferred = $q.defer();

                $http.defaults.headers.common.Authorization = 'Basic ' + btoa('hollyhaho@yahoo.com' + ':' + 'cacheme');
                $http.post('https://api-us.clusterpoint.com/100503' + '/tasks.json', task)
                    .success(function(data/*, status, headers, config*/) {
                        deferred.resolve(data);
                    })
                    .error(function(/*data, status, headers, config*/) {
                        deferred.reject();
                    })
                ;

                return deferred.promise;
            },
            update: function(task) {
                var deferred = $q.defer();

                $http.defaults.headers.common.Authorization = 'Basic ' + btoa('hollyhaho@yahoo.com' + ':' + 'cacheme');
                $http.put('https://api-us.clusterpoint.com/100503' + '/tasks.json', task)
                    .success(function(data/*, status, headers, config*/) {
                        deferred.resolve(data);
                    })
                    .error(function(/*data, status, headers, config*/) {
                        deferred.reject();
                    })
                ;

                return deferred.promise;
            },
            remove: function(taskId) {
                var deferred = $q.defer();

                $http.defaults.headers.common.Authorization = 'Basic ' + btoa('hollyhaho@yahoo.com' + ':' + 'cacheme');
                $http.delete('https://api-us.clusterpoint.com/100503' + '/tasks.json', { 'id': taskId })
                    .success(function(data/*, status, headers, config*/) {
                        deferred.resolve(data);
                    })
                    .error(function(/*data, status, headers, config*/) {
                        deferred.reject();
                    })
                ;

                return deferred.promise;
            },
            get: function(taskId) {
                var deferred = $q.defer();

                $http.defaults.headers.common.Authorization = 'Basic ' + btoa('hollyhaho@yahoo.com' + ':' + 'cacheme');
                $http.post('https://api-us.clusterpoint.com/100503' + '/tasks/_retrieve.json', { 'id': taskId })
                    .success(function(data/*, status, headers, config*/) {
                        deferred.resolve(data.documents[0]);
                    })
                    .error(function(/*data, status, headers, config*/) {
                        deferred.reject();
                    })
                ;

                return deferred.promise;
            },
            getMultiple: function(tasks) {
                var deferred = $q.defer();

                $http.defaults.headers.common.Authorization = 'Basic ' + btoa('hollyhaho@yahoo.com' + ':' + 'cacheme');
                $http.post('https://api-us.clusterpoint.com/100503' + '/tasks/_retrieve.json', tasks)
                    .success(function(data/*, status, headers, config*/) {
                        deferred.resolve(data);
                    })
                    .error(function(/*data, status, headers, config*/) {
                        deferred.reject();
                    })
                ;

                return deferred.promise;
            }
        };
    })

    .factory('Tasks', function() {
        // Might use a resource here that returns a JSON array

        // Some fake users
        var tasks = [{
            id: 0,
            challengeId: 0,
            taskNumber: 1,
            name: 'Design',
            description: 'Create the angry birds, pigs, and haystacks in illustrator',
            skills: 'Illustrator'

        },{
            id: 1,
            challengeId: 0,
            taskNumber: 2,
            name: 'Implement Code',
            description: 'Create a physics function for angry bird to successfully aim at the pigs',
            skills: 'Javascript'
        },{
            id: 2,
            challengeId: 0,
            taskNumber: 3,
            name: 'Implement Code',
            description: 'Create a function to make pigs disappear if bird successfully hits bird',
            skills: 'Javascript'
        },{
            id: 3,
            challengeId: 0,
            taskNumber: 4,
            name: 'Implement Code',
            description: 'Update the background using physics with the changes that result from the pigs disappearing',
            skills: 'Javascript'
        }];

        return {
            getForChallenge: function (challengeId) {
                var t = [];
                for (var i = 0; i < tasks.length; i++) {
                    if (tasks[i].challengeId === parseInt(challengeId)) {
                        t.push(tasks[i]);
                    }
                }
                return t;
            },
            all: function() {
                return tasks;
            },
            remove: function (task) {
                tasks.splice(tasks.indexOf(task), 1);
            },
            get: function (taskId) {
                for (var i = 0; i < tasks.length; i++) {
                    if (tasks[i].id === parseInt(taskId)) {
                        return tasks[i];
                    }
                }
                return null;
            }
        };
    })

    .factory('ChallengesAPI', function($q, $http) {
        return {
            getForProfession: function(userProfession) {
                var deferred = $q.defer();

                $http.defaults.headers.common.Authorization = 'Basic ' + btoa('hollyhaho@yahoo.com' + ':' + 'cacheme');
                $http.post('https://api-us.clusterpoint.com/100503' + '/challenges/_search.json', { 'query': '<professions>' + userProfession + '</professions>', 'list': { 'document': 'yes' } })
                    .success(function(data/*, status, headers, config*/) {
                        deferred.resolve(data);
                    })
                    .error(function(/*data, status, headers, config*/) {
                        deferred.reject();
                    })
                ;

                return deferred.promise;
            },
            all: function() {
                var deferred = $q.defer();

                $http.defaults.headers.common.Authorization = 'Basic ' + btoa('hollyhaho@yahoo.com' + ':' + 'cacheme');
                $http.post('https://api-us.clusterpoint.com/100503' + '/challenges/_search.json', { 'query': '*', 'list': { 'document': 'yes' } })
                    .success(function(data/*, status, headers, config*/) {
                        deferred.resolve(data);
                    })
                    .error(function(/*data, status, headers, config*/) {
                        deferred.reject();
                    })
                ;

                return deferred.promise;
            },
            insert: function(challenge) {
                var deferred = $q.defer();

                $http.defaults.headers.common.Authorization = 'Basic ' + btoa('hollyhaho@yahoo.com' + ':' + 'cacheme');
                $http.post('https://api-us.clusterpoint.com/100503' + '/challenges.json', challenge)
                    .success(function(data/*, status, headers, config*/) {
                        deferred.resolve(data);
                    })
                    .error(function(/*data, status, headers, config*/) {
                        deferred.reject();
                    })
                ;

                return deferred.promise;
            },
            update: function(challenge) {
                var deferred = $q.defer();

                $http.defaults.headers.common.Authorization = 'Basic ' + btoa('hollyhaho@yahoo.com' + ':' + 'cacheme');
                $http.put('https://api-us.clusterpoint.com/100503' + '/challenges.json', challenge)
                    .success(function(data/*, status, headers, config*/) {
                        deferred.resolve(data);
                    })
                    .error(function(/*data, status, headers, config*/) {
                        deferred.reject();
                    })
                ;

                return deferred.promise;
            },
            remove: function(challengeId) {
                var deferred = $q.defer();

                $http.defaults.headers.common.Authorization = 'Basic ' + btoa('hollyhaho@yahoo.com' + ':' + 'cacheme');
                $http.delete('https://api-us.clusterpoint.com/100503' + '/challenges.json', { 'id': challengeId })
                    .success(function(data/*, status, headers, config*/) {
                        deferred.resolve(data);
                    })
                    .error(function(/*data, status, headers, config*/) {
                        deferred.reject();
                    })
                ;

                return deferred.promise;
            },
            get: function(challengeId) {
                var deferred = $q.defer();

                $http.defaults.headers.common.Authorization = 'Basic ' + btoa('hollyhaho@yahoo.com' + ':' + 'cacheme');
                $http.post('https://api-us.clusterpoint.com/100503' + '/challenges/_retrieve.json', { 'id': challengeId })
                    .success(function(data/*, status, headers, config*/) {
                        deferred.resolve(data.documents[0]);
                    })
                    .error(function(/*data, status, headers, config*/) {
                        deferred.reject();
                    })
                ;

                return deferred.promise;
            },
            getMultiple: function(challenges) {
                var deferred = $q.defer();

                $http.defaults.headers.common.Authorization = 'Basic ' + btoa('hollyhaho@yahoo.com' + ':' + 'cacheme');
                $http.post('https://api-us.clusterpoint.com/100503' + '/challenges/_retrieve.json', challenges)
                    .success(function(data/*, status, headers, config*/) {
                        deferred.resolve(data);
                    })
                    .error(function(/*data, status, headers, config*/) {
                        deferred.reject();
                    })
                ;

                return deferred.promise;
            }
        };
    })

    .factory('Challenges', function() {
        // Might use a resource here that returns a JSON array

        // Some fake users
        var challenges = [{
            id: 0,
            name: 'Angry Bird',
            description: 'Create the famous Angry Bird game! Angry birds is a simple game that you can implement! Learn basic html, css, and javascript to make the birds attack the pigs or learn illustrator and photoshop to create the graphics in this game.',
            professions: ['Designer','Developer']
            //skillsNeeded: ['HTML', 'CSS', 'PHP', 'Javascript', 'Illustrator']
        }, {
            id: 1,
            name: 'Build Facebook',
            description: 'Build the earliest version of Facebook and create the social media that connects 1.45 billion people across the globe! Learn the algorithm that comes up with your friend recommendations and the business logic behind the advertisements!',
            professions: ['Designer','Developer','Manager']
            //skillsNeeded: ['HTML', 'CSS', 'PHP', 'Javascript', 'Illustrator']
        }];

        return {
            getForProfession: function(userProfession) {
                var c = [];
                for (var i = 0; i < challenges.length; i++)
                {
                    for (var j = 0; j < challenges.professions.length, j++;)
                    {
                        if (userProfession === challenges.professions[j]) {
                            c.push(challenges[i]);
                        }
                    }
                }
                return c;
            },
            all: function() {
                return challenges;
            },
            remove: function(challenge) {
                challenges.splice(challenges.indexOf(challenge), 1);
            },
            get: function(challengeId) {
                for (var i = 0; i < challenges.length; i++) {
                    if (challenges[i].id === parseInt(challengeId)) {
                        return challenges[i];
                    }
                }
                return null;
            }
        };
    })

    .factory('SkillsAPI', function($q, $http) {
        return {
            getForUser: function(userId) {
                var deferred = $q.defer();

                $http.defaults.headers.common.Authorization = 'Basic ' + btoa('hollyhaho@yahoo.com' + ':' + 'cacheme');
                $http.post('https://api-us.clusterpoint.com/100503' + '/skills/_search.json', { 'query': '<userId>' + userId + '</userId>', 'list': { 'document': 'yes' } })
                    .success(function(data/*, status, headers, config*/) {
                        deferred.resolve(data);
                    })
                    .error(function(/*data, status, headers, config*/) {
                        deferred.reject();
                    })
                ;

                return deferred.promise;
            },
            all: function() {
                var deferred = $q.defer();

                $http.defaults.headers.common.Authorization = 'Basic ' + btoa('hollyhaho@yahoo.com' + ':' + 'cacheme');
                $http.post('https://api-us.clusterpoint.com/100503' + '/skills/_search.json', { 'query': '*', 'list': { 'document': 'yes' } })
                    .success(function(data/*, status, headers, config*/) {
                        deferred.resolve(data);
                    })
                    .error(function(/*data, status, headers, config*/) {
                        deferred.reject();
                    })
                ;

                return deferred.promise;
            },
            insert: function(skill) {
                var deferred = $q.defer();

                $http.defaults.headers.common.Authorization = 'Basic ' + btoa('hollyhaho@yahoo.com' + ':' + 'cacheme');
                $http.post('https://api-us.clusterpoint.com/100503' + '/skills.json', skill)
                    .success(function(data/*, status, headers, config*/) {
                        deferred.resolve(data);
                    })
                    .error(function(/*data, status, headers, config*/) {
                        deferred.reject();
                    })
                ;

                return deferred.promise;
            },
            update: function(skill) {
                var deferred = $q.defer();

                $http.defaults.headers.common.Authorization = 'Basic ' + btoa('hollyhaho@yahoo.com' + ':' + 'cacheme');
                $http.put('https://api-us.clusterpoint.com/100503' + '/skills.json', skill)
                    .success(function(data/*, status, headers, config*/) {
                        deferred.resolve(data);
                    })
                    .error(function(/*data, status, headers, config*/) {
                        deferred.reject();
                    })
                ;

                return deferred.promise;
            },
            remove: function(skillId) {
                var deferred = $q.defer();

                $http.defaults.headers.common.Authorization = 'Basic ' + btoa('hollyhaho@yahoo.com' + ':' + 'cacheme');
                $http.delete('https://api-us.clusterpoint.com/100503' + '/skills.json', { 'id': skillId })
                    .success(function(data/*, status, headers, config*/) {
                        deferred.resolve(data);
                    })
                    .error(function(/*data, status, headers, config*/) {
                        deferred.reject();
                    })
                ;

                return deferred.promise;
            },
            get: function(skillId) {
                var deferred = $q.defer();

                $http.defaults.headers.common.Authorization = 'Basic ' + btoa('hollyhaho@yahoo.com' + ':' + 'cacheme');
                $http.post('https://api-us.clusterpoint.com/100503' + '/skills/_retrieve.json', { 'id': skillId })
                    .success(function(data/*, status, headers, config*/) {
                        deferred.resolve(data.documents[0]);
                    })
                    .error(function(/*data, status, headers, config*/) {
                        deferred.reject();
                    })
                ;

                return deferred.promise;
            },
            getMultiple: function(skills) {
                var deferred = $q.defer();

                $http.defaults.headers.common.Authorization = 'Basic ' + btoa('hollyhaho@yahoo.com' + ':' + 'cacheme');
                $http.post('https://api-us.clusterpoint.com/100503' + '/skills/_retrieve.json', skills)
                    .success(function(data/*, status, headers, config*/) {
                        deferred.resolve(data);
                    })
                    .error(function(/*data, status, headers, config*/) {
                        deferred.reject();
                    })
                ;

                return deferred.promise;
            }
        };
    })

        .factory('Skills', function() {
            // Might use a resource here that returns a JSON array

            // Some fake users
            var skills = [{
                id: 0,
                userId:0,
                slug: 'html',
                name: 'HTML'
            }, {
                id: 1,
                userId:0,
                slug: 'css',
                name: 'CSS'
            }, {
                id: 2,
                userId: 0,
                slug: 'php',
                name: 'PHP'
            },{
                id: 4,
                userId: 0,
                slug: 'javascript',
                name: 'Javascript'

            },{
                id: 5,
                userId: 1,
                slug: 'illustrator',
                name: 'Illustrator'

            },{
                id: 6,
                userId: 1,
                slug: 'photoshop',
                name: 'Photoshop'

            },{
                id: 7,
                userId: 2,
                slug: 'social-media',
                name: 'Social Media'

            },{
                id: 8,
                userId: 2,
                slug: 'seo',
                name: 'SEO'
            },{
                id: 9,
                userId: 3,
                slug: 'c++',
                name: 'C++'

            },{
                id: 10,
                userId: 3,
                slug: 'algorithm-design',
                name: 'Algorithm Design'

            },{
                id: 11,
                userId: 3,
                slug: 'html',
                name: 'HTML'

            },{
                id: 12,
                userId: 3,
                slug: 'css',
                name: 'CSS'

            }];

            return {
                getForUser: function(userId) {
                    var s = [];
                    for (var i = 0; i < skills.length; i++)
                    {
                        if (skills[i].userId === parseInt(userId))
                        {
                          s.push(skills[i]);
                        }
                    }
                    return s;
                },
                all: function() {
                    return skills;
                },
                remove: function(skill) {
                    skills.splice(skills.indexOf(skill), 1);
                },
                get: function(skillId) {
                    for (var i = 0; i < skills.length; i++) {
                        if (skills[i].id === parseInt(skillId)) {
                            return skills[i];
                        }
                    }
                    return null;
                }
            };
    })

    .factory('DatabasePopulator', function(UsersAPI, Users, TasksAPI, Tasks, ChallengesAPI, Challenges, SkillsAPI, Skills) {
        return {
            getIds: function(array) {
                var idObjects = [];
                for (var i = 0; i < array.length; i++)
                {
                    if (array[i].id || array[i].id === 0)
                    {
                        idObjects.push({ 'id': array[i].id });
                    }
                    else if (array[i].challengeId || array[i].challengeId === 0)
                    {
                        idObjects.push({ 'id': array[i].challengeId });
                    }
                    else if (array[i].userId || array[i].userId === 0)
                    {
                        idObjects.push({ 'id': array[i].userId });
                    }
                }
                return idObjects;
            },
            addObjectById: function(original, elementName, objects) {
                if (original !== undefined && objects !== undefined) {
                    for (var i = 0; i < original.length; i++)
                    {
                        for (var j = 0; j < objects.length; j++)
                        {
                            if (objects[j].id === original[i].id
                                || objects[j].id === original[i].challengeId
                                || objects[j].id === original[i].userId
                            ) {
                                original[i][elementName] = objects[j];
                            }
                        }
                    }
                }
                return original;
            },
            populateUsers: function() {
                UsersAPI.all().then(function(existingList) {
                    if (existingList.documents) {
                        UsersAPI.remove(existingList.documents).then(function () {
                            UsersAPI.insert(Users.all());
                        });
                    } else {
                        UsersAPI.insert(Users.all());
                    }
                });
            },
            populateTasks: function() {
                TasksAPI.all().then(function(existingList) {
                    if (existingList.documents) {
                        TasksAPI.remove(existingList.documents).then(function () {
                            TasksAPI.insert(Tasks.all());
                        });
                    } else {
                        TasksAPI.insert(Tasks.all());
                    }
                });
            },
            populateChallenges: function() {
                ChallengesAPI.all().then(function(existingList) {
                    if (existingList.documents) {
                        ChallengesAPI.remove(existingList.documents).then(function () {
                            ChallengesAPI.insert(Challenges.all());
                        });
                    } else {
                        ChallengesAPI.insert(Challenges.all());
                    }
                });
            },
            populateSkills: function() {
                SkillsAPI.all().then(function(existingList) {
                    if (existingList.documents) {
                        SkillsAPI.remove(existingList.documents).then(function () {
                            SkillsAPI.insert(Skills.all());
                        });
                    } else {
                        SkillsAPI.insert(Skills.all());
                    }
                });
            }
        };
    });

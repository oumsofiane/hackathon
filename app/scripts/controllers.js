'use strict';

angular.module('starter.controllers', [])

    .controller('ChalCtrl', function($scope, UsersAPI, ChallengesAPI, DatabasePopulator) {

    //DatabasePopulator.populateUsers();DatabasePopulator.populateTasks();DatabasePopulator.populateChallenges();DatabasePopulator.populateSkills();
    // Test User
    var userId = 0;

    UsersAPI.get(userId).then(function(user) {

        //var usersChallenges = Users.getChallenges(0);
        var usersChallenges = user.challenges;
        var challenges = [];

        for (var i = 0; i < usersChallenges.length; i++)
        {
            var status = '';
            var statusSlug = usersChallenges[i].status;
            if (usersChallenges[i].status === 'in-progress')
            {
                status = 'In Progress';
            }
            else if (usersChallenges[i].status === 'completed')
            {
                status = '';
            }
            else
            {
                status = 'Uncompleted';
            }
            usersChallenges[i].statusSlug = statusSlug;
            usersChallenges[i].status = status;

            //var challenge = Challenges.get(usersChallenges[i].challengeId);
            //challenges.push({challenge: challenge, status: status, statusSlug: statusSlug});
        }
        //$scope.challenges = challenges;
        ChallengesAPI.getMultiple(DatabasePopulator.getIds(usersChallenges)).then(function(challengeList) {
            $scope.challenges = DatabasePopulator.addObjectById(usersChallenges, 'challenge', challengeList.documents);
        });

    });

    })


//.controller('DashCtrl', function($scope) {})

.controller('TasksCtrl', function($scope, Users, Tasks, Challenges, UsersAPI, TasksAPI, ChallengesAPI){
        // Test User
        var userId = 1;
        //$scope.user = Users.get(userId);

    UsersAPI.get(userId).then(function(user) {
        $scope.user = user;

        $scope.tasks = [];
        var userTasks = $scope.user.ownTasks;

        userTasks.forEach(function(entry) {
            //var t = Tasks.get(entry.id);
            //var a = Users.get(entry.assignedByUser);
            //var c = Challenges.get(t.challengeId);

            TasksAPI.get(entry.id).then(function(t) {
                UsersAPI.get(entry.assignedByUser).then(function(a) {
                    ChallengesAPI.get(t.challengeId).then(function(c) {
                        $scope.tasks.push({task: t, assigner: a, challenge: c, userTask: entry});
                    });
                });
            });

            //$scope.tasks.push({task: t, assigner:a, challenge: c, userTask: entry});
        });


        $scope.delete = function(task){
            //Users.deleteTask($scope.user, task);
            UsersAPI.deleteTask($scope.user, task);
        };
    });

})

.controller('TaskDetailCtrl', function($scope, $stateParams, $state, Users, UsersAPI, Tasks, Configuration) {
    console.log(Configuration.all());
    // get current user
    var userId = Configuration.all().user;
    var user = UsersAPI.get(userId).then(function(user){
        console.log(user);
        $scope.user = user;
        //get task
        var taskId = $stateParams.taskId;
        var task = Tasks.get(taskId);
        console.log(task);

        var userTask = Users.getUserTask(user, task);
        console.log(userTask);

        $scope.task = task;
        $scope.userTask = userTask;

        $scope.yes = function() {
            $scope.userTask.status = 'in-progress'
            console.log("Function Yes on TaskDetailCtrl Called");
            console.log($state);
            $state.go('tab.task-attempt', {'userTask': $scope.userTask, task: $scope.task});
        };
        $scope.no = function() {
            UsersAPI.deleteTask($scope.user, $scope.userTask);
            console.log("Function No on TaskDetailCtrl Called");
            $state.go('tab.tasks');
        };
    });
})


    .controller('TaskAttemptCtrl', function($scope, $stateParams, $state, Users, UsersAPI, Tasks, Configuration) {
        console.log('TaskAttemptCtrl');
        console.log(Configuration.all());
        // get current user
        var userId = Configuration.all().user;
        console.log($stateParams);
        var user = UsersAPI.get(userId).then(function(user){
            console.log($stateParams.userTask);
            console.log($state);

            console.log("TaskAttemptCtrl");
            $scope.user = user;
            $scope.task = $stateParams.task;
            $scope.userTask = $stateParams.userTask;

            console.log($scope.user);
            console.log($scope.userTask);
            console.log($scope.task);

            $scope.resign = function() {
                UsersAPI.deleteTask($scope.user, $scope.userTask);
                console.log("Function No on TaskDetailCtrl Called");
                $state.go('tab.tasks');
            };
        });
    })

    .controller('ChalTaskAttemptCtrl', function($scope, $stateParams, $state, Users, UsersAPI, Tasks, Configuration) {
        console.log('ChalTaskAttemptCtrl');
        console.log(Configuration.all());
        // get current user
        var userId = Configuration.all().user;
        console.log($stateParams);
        var user = UsersAPI.get(userId).then(function(user){
            console.log($stateParams.userTask);
            console.log($state);

            console.log("TaskAttemptCtrl");
            $scope.user = user;
            $scope.task = $stateParams.task;
            $scope.userTask = $stateParams.userTask;

            console.log($scope.user);
            console.log($scope.userTask);
            console.log($scope.task);

            $scope.resign = function() {
                UsersAPI.deleteTask($scope.user, $scope.userTask);
                console.log("Function No on TaskDetailCtrl Called");
                $state.go('tab.chal-detail');
            };
        });
    })

    .controller('ConnectionsCtrl', function($scope, Users, UsersAPI, DatabasePopulator){
        var userId = 0;
        //var usersConnections = Users.getConnections(userId);

        UsersAPI.get(userId).then(function(user) {

            /*
        var connections = [];
        for (var i = 0; i < usersConnections.length; i++)
        {
            var status = usersConnections[i].status;
            var user = Users.get(usersConnections[i].userId);
            connections.push({user: user, status: status});
        }
        $scope.connections = connections;
            */

            UsersAPI.getMultiple(DatabasePopulator.getIds(user.connections)).then(function(connectionList) {
                $scope.connections = DatabasePopulator.addObjectById(user.connections, 'user', connectionList.documents);
            });

        });
    })

    .controller('ResumeCtrl', function ($scope, $stateParams, Users, Skills, UsersAPI, SkillsAPI, DatabasePopulator)
    {
        var userId = 0;
        //var user = Users.get(userId);

        UsersAPI.get(userId).then(function(user) {


        $scope.user = user;

            /*
        var skills=[];
        for (var i = 0; i < user.skills.length; i++)
        {
            var level = user.skills[i].level;
            var skill = Skills.get(user.skills[i].id);
            skills.push({skill: skill,level : level });
        }
        $scope.skills = skills;
        $scope.user = user;
            */

            SkillsAPI.getMultiple(DatabasePopulator.getIds(user.skills)).then(function(skillList) {
                $scope.skills = DatabasePopulator.addObjectById(user.skills, 'skill', skillList.documents);
            });

        });
    })

    .controller('ChalDetailCtrl', function($scope, $stateParams, $state, Challenges, Tasks, Users, ChallengesAPI, TasksAPI, UsersAPI)
    {
        //var challenge = Challenges.get($stateParams.chalId);
        ChallengesAPI.get($stateParams.chalId).then(function(challenge) {
            $scope.challenge = challenge;
        });

        var tasks = Tasks.getForChallenge($stateParams.chalId);
        TasksAPI.getForChallenge($stateParams.chalId).then(function(taskList) {
            var tasks = taskList.documents;


        var userId = 0;
        //var user = Users.get(userId);
            UsersAPI.get(userId).then(function(user) {

        var delegatedTasks = user.delegatedTasks;
        var ownTasks = user.ownTasks;
        for (var i = 0; i < tasks.length; i++)
        {
            for (var j = 0; j < delegatedTasks.length; j++) {
                if (delegatedTasks[j].id === tasks[i].id)
                {
                    tasks[i].status= 'delegate';
                }
                else{
                    tasks[i].status= 'none';
                }
            }

            for (var k = 0; k < ownTasks.length; k++)
            {
                if (delegatedTasks[k].id === tasks[i].id)
                {
                    tasks[i].status= 'attempt';
                }
            }
            console.log(tasks[i].status);
        }
        $scope.tasks = tasks;
        //$scope.challenge = challenge;
        $scope.attempt = function(task){
            console.log("attempt in challenge details");
            $state.go('tab.chal-task-attempt', {task: task, userTask: task, chalId: $stateParams.chalId});
        };
            });
        });

        console.log($scope);
    })

    .controller('ChooseConnectionCtrl', function($scope, $stateParams, Tasks, Users, UsersAPI, TasksAPI, DatabasePopulator){
        var userId = 0;
        //var usersConnections = Users.getConnections(userId);

        $scope.delegateTask = function(task, user, assigner) {
            console.log(task);
            console.log(user);
            console.log(assigner);
            Users.delegateTask(task,user, assigner);
            $window.history.back();
        };


        UsersAPI.get(userId).then(function(user) {
            var userConnections = user.connections;
            $scope.user = user;

        //var task = Tasks.get($stateParams.taskId);

            TasksAPI.get($stateParams.taskId).then(function (task) {

                /*
        var connections = [];
        for (var i = 0; i < usersConnections.length; i++)
        {
            if (usersConnections.status)
            {
                var user = Users.get(usersConnections[i].userId);
                for (var j = 0; j < task.skills.length; j++)
                {
                    if (user.profession === task.skills[i])
                    {
                        connections.push({user: user, status:usersConnections[i].status });
                    }
                }
            }
        }
        $scope.connections = connections;
                */
                $scope.task = task;

                UsersAPI.getMultiple(DatabasePopulator.getIds(userConnections)).then(function(connectionList) {
                    var dbConnections = DatabasePopulator.addObjectById(userConnections, 'user', connectionList.documents);
                    /*
                    var connections = [];
                    for (var i = 0; i < dbConnections.length; i++)
                    {
                        if (dbConnections[i].status)
                        {
                            var user = dbConnections[i].user;
                            for (var j = 0; j < task.skills.length; j++)
                            {
                                if (user.profession === task.skills[i])
                                {
                                    connections.push(dbConnections[i]);
                                }
                            }
                        }
                    }
                    $scope.connections = connections;
                    */
                    $scope.connections = dbConnections;
                });

            });
        });
    })


    .controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
        $scope.chat = Chats.get($stateParams.chatId);
    })

    .controller('AccountCtrl', function($scope) {
        $scope.settings = {
            enableFriends: true
        };
    });
